#!/bin/sh
APP_VERSION=2.3.1
APP_NAME=xxl-job-admin-2.3.1.jar
LOG_ROOT=/opt/logs

#使用说明，用来提示输入参数
usage() {
    echo "Usage: sh scriptName.sh [start|stop|restart|status]"
    exit 1
}

#检查程序是否在运行
is_exist(){
  pid=`ps -ef|grep $APP_NAME|grep -v grep|awk '{print $2}' `
  #如果不存在返回1，存在返回0
  if [ -z "${pid}" ]; then
   return 1
  else
    return 0
  fi
}

#启动方法
start(){
  is_exist
  if [ $? -eq "0" ]; then
     echo "${APP_NAME} is already running. pid=${pid} ."
  else
     echo "${APP_NAME} is not running."
      nohup java -server -verbose:gc -Xloggc:./log/gc_`date +%Y%m%d%H%M%S`.log \
      -XX:+DisableExplicitGC -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintHeapAtGC -XX:+PrintGCTimeStamps -XX:+PrintClassHistogram \
      -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=./log -XX:ErrorFile=./log/hs_err_%p.log \
      -XX:+UseG1GC -Xms2048m -Xmx2048m -XX:MaxMetaspaceSize=1024m -XX:MetaspaceSize=1024m -jar -Dlogging.file=$LOG_ROOT/xxl-job-admin.log ${APP_NAME}  > output 2>&1  &
    echo "${APP_NAME} start success"
  fi
}

#停止方法
stop(){
  is_exist
  if [ $? -eq "0" ]; then
    kill -9 $pid
  else
    echo "${APP_NAME} is not running"
  fi
}

#输出运行状态
status(){
  is_exist
  if [ $? -eq "0" ]; then
    echo "${APP_NAME} is running. Pid is ${pid}"
  else
    echo "${APP_NAME} is NOT running."
  fi
}

#重启
restart(){
  stop
  start
}

#根据输入参数，选择执行对应方法，不输入则执行使用说明
case "$1" in
  "start")
    start
    ;;
  "stop")
    stop
    ;;
  "status")
    status
    ;;
  "restart")
    restart
    ;;
  *)
    usage
    ;;
esac

