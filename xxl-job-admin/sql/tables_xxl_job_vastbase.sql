CREATE database xxl_job with tablespace=pg_default;


-- public.xxl_job_info definition

-- Drop table

DROP TABLE xxl_job_info;

CREATE TABLE xxl_job_info (
	id serial4 NOT NULL,
	job_group int8 NOT NULL,
	job_desc varchar(255) NOT NULL,
	add_time timestamp NULL,
	update_time timestamp NULL,
	author varchar(64) NULL,
	alarm_email varchar(255) NULL,
	schedule_type varchar(50) NOT NULL DEFAULT 'NONE'::varchar,
	schedule_conf varchar(128) NULL,
	misfire_strategy varchar(50) NOT NULL DEFAULT 'DO_NOTHING'::varchar,
	executor_route_strategy varchar(50) NULL,
	executor_handler varchar(255) NULL,
	executor_param varchar(512) NULL,
	executor_block_strategy varchar(50) NULL,
	executor_timeout int8 NOT NULL DEFAULT 0,
	executor_fail_retry_count varchar NOT NULL DEFAULT 0,
	glue_type varchar(50) NOT NULL,
	glue_source text NULL,
	glue_remark varchar(128) NOT NULL,
	glue_updatetime timestamp NULL,
	child_jobid varchar(255) NULL,
	trigger_status int2 NOT NULL DEFAULT 0,
	trigger_last_time int8 NOT NULL DEFAULT 0,
	trigger_next_time int8 NOT NULL DEFAULT 0,
	CONSTRAINT xxl_job_info_pk PRIMARY KEY (id)
);

CREATE TABLE xxl_job_log (
	id serial4 NOT NULL,
	job_group int8 NOT NULL,
	job_id int8 NOT NULL,
	executor_address varchar(255) NULL,
	executor_handler varchar(255) NULL,
	executor_param varchar(512) NULL,
	executor_sharding_param varchar(20) NULL,
	executor_fail_retry_count int8 NOT NULL DEFAULT 0,
	trigger_time timestamp NULL,
	trigger_code int8 NOT NULL,
	trigger_msg text NULL,
	handle_time timestamp NULL,
	handle_code int8 NOT NULL,
	handle_msg text NULL,
	alarm_status int2 NOT NULL DEFAULT 0,
	CONSTRAINT xxl_job_log_pk PRIMARY KEY (id)
);
CREATE INDEX idx_alarm_status_handle_code ON xxl_schema.xxl_job_log USING btree (alarm_status, handle_code, trigger_code);

CREATE TABLE xxl_job_log_report (
	id serial4 NOT NULL,
	trigger_day timestamp NULL,
	running_count int8 NOT NULL DEFAULT 0,
	suc_count int8 NOT NULL DEFAULT 0,
	fail_count int8 NOT NULL DEFAULT 0,
	update_time timestamp NULL,
	CONSTRAINT i_trigger_day UNIQUE (trigger_day),
	CONSTRAINT xxl_job_log_report_pk PRIMARY KEY (id)
);

CREATE TABLE xxl_job_logglue (
	id serial4 NOT NULL,
	job_id int8 NOT NULL,
	glue_type varchar(50) NULL,
	glue_source text NULL,
	glue_remark varchar(128) NOT NULL,
	add_time timestamp NULL,
	update_time timestamp NULL,
	CONSTRAINT xxl_job_logglue_pk PRIMARY KEY (id)
);

CREATE TABLE xxl_job_registry (
	id serial4 NOT NULL,
	registry_group varchar(50) NOT NULL,
	registry_key varchar(255) NOT NULL,
	registry_value varchar(255) NOT NULL,
	update_time timestamp NULL,
	CONSTRAINT xxl_job_registry_pk PRIMARY KEY (id)
);
CREATE INDEX i_g_k_v ON xxl_schema.xxl_job_registry USING btree (registry_group, registry_key, registry_value);

CREATE TABLE xxl_job_group (
	id serial4 NOT NULL,
	app_name varchar(64) NOT NULL,
	title varchar(512) NOT NULL,
	address_type int2 NOT NULL DEFAULT 0,
	address_list text NULL,
	update_time timestamp NULL,
	CONSTRAINT xxl_job_group_pk PRIMARY KEY (id)
);

CREATE TABLE xxl_job_user (
	id serial4 NOT NULL,
	username varchar(50) NOT NULL,
	"password" varchar(50) NOT NULL,
	"role" int2 NOT NULL,
	"permission" varchar(255) NULL,
	CONSTRAINT i_username UNIQUE (username),
	CONSTRAINT xxl_job_user_pk PRIMARY KEY (id)
);

CREATE TABLE xxl_job_lock (
	lock_name varchar(50) NOT NULL,
	CONSTRAINT xxl_job_lock_pk PRIMARY KEY (lock_name)
);